package id.bootcamp.batch330android.registration

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.bootcamp.batch330android.R
import org.intellij.lang.annotations.Language

class RegistrationResultActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        //Ambil data yang di kirim dari activity sebelumnya
        val fisrtName = intent.extras?.getString("keyFirstName")
        val lastName = intent.extras?.getString("keyLastName")
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val gender= intent.extras?.getString("keyGender")
        val language= intent.extras?.getStringArrayList("keyLanguage")
        val state= intent.extras?.getString("keyState")


        //Ambil objek dari layout(Pastikan id sesuai)
        val textfirstName =findViewById<TextView>(R.id.textfirtsName)
        val textlastName =findViewById<TextView>(R.id.textlastName)
        val textEmail =findViewById<TextView>(R.id.textEmail)
        val textAddress =findViewById<TextView>(R.id.textAddress)
        val textGender=findViewById<TextView>(R.id.textGender)
        val textLanguage=findViewById<TextView>(R.id.textLanguage)
        val textState =findViewById<TextView>(R.id.textState)

        //set text data dari activity sebelumnya
        textfirstName.text="Firts Name : $fisrtName"
        textlastName.text="Last Name : $lastName"
        textEmail.text="Email : $email"
        textAddress.text="Address : $address"
        textGender.text="Gender : $gender"
        textLanguage.text="Language : $language"
        textState.text="State : $state"
    }
}