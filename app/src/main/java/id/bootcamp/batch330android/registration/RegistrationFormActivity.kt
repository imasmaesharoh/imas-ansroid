package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import id.bootcamp.batch330android.R
import id.bootcamp.batch330android.Utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Perintah untuk memasang Layout
        setContentView(R.layout.activity_registration_form)

        //Ambil semua objek yang ada di form
        val editTextFirstName =findViewById<EditText>(R.id.etFirstName)
        val editTextLastName = findViewById<EditText>(R.id.etLastName)
        val radioMale = findViewById<RadioButton>(R.id.radioMale)
        val radioFemale= findViewById<RadioButton>(R.id.radioFemale)
        val editTextEmail = findViewById<EditText>(R.id.etEmail)
        val editTextAddress = findViewById<EditText>(R.id.etAddress)
        val cbIndonesia = findViewById<CheckBox>(R.id.cbIndonesia)
        val cbInggris = findViewById<CheckBox>(R.id.cbEnglish)
        val spinState= findViewById<Spinner>(R.id.spinnerState)
        val btnSubmit = findViewById<Button>(R.id.btnSubmit)

        //Ambil Array String dari Resource
        val states = resources.getStringArray(R.array.states)

        //Mengisi Spinner/dropdown
        val adapter =ArrayAdapter(this,
            android.R.layout.simple_list_item_1,states)

        spinState.adapter = adapter

        //Buat clickListener button submit
        btnSubmit.setOnClickListener {
            //Perintah mengambol value dari form
            //kepanggil saat user memasukan datanya/ seluruh data

            //Edit text
            val firstName= editTextFirstName.text.toString()
            val lastName= editTextLastName.text.toString()
            val address= editTextAddress.text.toString()
            val email= editTextEmail.text.toString()

            //radio buttton
            var gender =""
            if (radioMale.isChecked){
                gender="Male"
            }else if (radioFemale.isChecked){
                gender="Female"
            }

            //Checkbox
            //printah deklarasi membuat list kosong
            var lisLanguage = ArrayList<String>()
            if(cbIndonesia.isChecked){
                //perintah buat menamvahkan data di lisLanguage
                lisLanguage.add("Indonesia")
            }
            if (cbInggris.isChecked){
                lisLanguage.add("English")
            }

            //Spinner
            val state = spinState.selectedItem.toString()

            //Logic Validation
            //First Name tidak boleh kosong
            if (firstName.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                "First Name tidak boleh kosong",
                Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //Email Haarus valid
            val isEmailValid = isValidEmail(email)
            if (isEmailValid==false){
                Toast.makeText(this@RegistrationFormActivity,
                    "Email tidak Valid",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //address tidak boleh kosong
            if (address.isBlank()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Address tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //Gendre Harus di pilij
            if (gender.isEmpty()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Gender tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //langage harus dipilih minikmal 1
            if(lisLanguage.isEmpty()){
                Toast.makeText(this@RegistrationFormActivity,
                    "Language tidak boleh kosong",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            val intent =Intent(this,RegistrationResultActivity:: class.java)

            intent.putExtra("keyFirstName",firstName)
            intent.putExtra("keyLastName",lastName)
            intent.putExtra("keyEmail",email)
            intent.putExtra("keyAddress",address)
            intent.putExtra("keyGender",gender)
            intent.putExtra("keyLanguage",lisLanguage)
            intent.putExtra("keyState",state)
            startActivity(intent)

        }
    }
}