package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import id.bootcamp.batch330android.R

class Stack1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack1)

        //Ambil object button
        val btnGoStack2Activity = findViewById<Button>(R.id.btnGoStack2)

        //Atur button setelah di click
        btnGoStack2Activity.setOnClickListener {
            //Buat object Intent
            //Untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, Stack2Activity::class.java)
            startActivity(intent)
        }
    }
}