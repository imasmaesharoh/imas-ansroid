package com.example.mytablayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private val arrTitle = arrayListOf("Home","Profile")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = SectionPagerAdapter(this)
        val viewPager :ViewPager2 = findViewById(R.id.viewPager)
        val tabLayout :TabLayout= findViewById(R.id.tabLayout)

        viewPager.adapter =adapter
        viewPager.adapter =adapter
        TabLayoutMediator(tabLayout, viewPager){
                tab,position ->
            tab.text =arrTitle[position]
        }.attach()
    }
}