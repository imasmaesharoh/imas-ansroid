package com.example.mainnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.mainnavigation.databinding.FragmentCategoryBinding
import com.example.mainnavigation.databinding.FragmentHomeBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment() {
    lateinit var  binding: FragmentCategoryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      binding = FragmentCategoryBinding.inflate(inflater,container,false)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLife.setOnClickListener{
            val bundle =Bundle()
            bundle.putString(DetailCatagoryFragment.EXTRA_NAME,"My life Style")
            bundle.putString(DetailCatagoryFragment.EXTRA_DESC,"My life Style is frugel")
            view.findNavController()
                .navigate(R.id.action_categoryFragment_to_detailCatagoryFragment,bundle)
        }
    }

}