package com.example.mainnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.mainnavigation.databinding.FragmentDetailCatagoryBinding
import com.example.mainnavigation.databinding.FragmentHomeBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailCatagoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailCatagoryFragment : Fragment() {

    lateinit var binding: FragmentDetailCatagoryBinding

    companion object{
        val EXTRA_NAME ="EXTRA_NAME"
        val EXTRA_DESC = "EXTRA_DESC"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         val dataName = arguments?.getString(EXTRA_NAME)
        val dataDesc = arguments?.getString(EXTRA_DESC)

        binding.tvName.text = dataName
        binding.tvDesc.text =dataDesc

        binding.btnToHome.setOnClickListener {
            it.findNavController().navigate(R.id.action_detailCatagoryFragment_to_homeFragment)
        }
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =FragmentDetailCatagoryBinding.inflate(inflater,container,false)
        return binding.root
    }

}