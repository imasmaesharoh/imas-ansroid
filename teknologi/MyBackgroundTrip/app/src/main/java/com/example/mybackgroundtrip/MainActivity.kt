package com.example.mybackgroundtrip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStart = findViewById<Button>(R.id.btnStart)
        val tvStatus = findViewById<TextView>(R.id.tvStatus)
        //cara kotlin (Corotin)
        btnStart.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Default){
                for (i in 0 .. 10){
                    delay(500)
                    val precentage = i *10
                    withContext(Dispatchers.Main) {

                        if (precentage == 100) {
                            tvStatus.text = "Task Complete"
                        } else {
                            tvStatus.text = "Task Proses $precentage%"
                        }
                    }
                }
            }
        }


        //Cara java &kotlin (Excutor)

//        val executor = Executors.newSingleThreadExecutor()
//        val handler = Handler(Looper.getMainLooper())
//
//        btnStart.setOnClickListener {
//            //proses 10 detik
//            executor.execute {
//                for (i in 0 .. 10){
//                    Thread.sleep(500)
//                    val parcentage = i*10
//                    handler.post {
//                        if (parcentage == 100){
//                            tvStatus.text ="Task Complete"
//                        }else{
//                            tvStatus.text="Task proses $parcentage%"
//                        }
//
//                    }
//
//                }
//            }
//
//        }
    }
}