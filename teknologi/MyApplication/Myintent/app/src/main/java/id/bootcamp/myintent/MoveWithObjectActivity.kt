package id.bootcamp.myintent

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MoveWithObjectActivity : AppCompatActivity() {
    companion object{
        const val KEY_OBJECT ="key_object"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_object)

        val tvHasil = findViewById<TextView>(R.id.tvHasil)

        var person:Person?
        if (Build.VERSION.SDK_INT >= 33){
             person =intent.getParcelableExtra(KEY_OBJECT,Person::class.java)
        }else{
             person =intent.getParcelableExtra<Person>(KEY_OBJECT)
        }

        if (person != null){
            val text ="Name : ${person.name}"+", Age : ${person.age}"+
                    ", Email :${person.email}"+ ", Address :${person.city} "
            tvHasil.text=text
        }
    }
}