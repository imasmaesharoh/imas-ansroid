package id.bootcamp.myintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MoveWithDataActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_AGE ="ekstra_age"
        const val EXTRA_NAME ="ekstra_name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_data)

        val tvname:TextView = findViewById (R.id.name)
        val tvage:TextView = findViewById (R.id.age)

        val name = intent.getStringExtra(EXTRA_NAME)
        val age = intent.getIntExtra(EXTRA_AGE, 0)

        val text ="$name"
        tvname.text = text

        val text1 ="$age"
        tvage.text =text1
    }
}