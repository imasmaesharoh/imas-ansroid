package id.bootcamp.myintent
import  android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var tvResult :TextView
    //digunakan untuk mengakap data jika ingin menerima
    //balikan dari activity selanjutnya

    private val resultLauncher =registerForActivityResult( //balikan data ke main activity
        ActivityResultContracts.StartActivityForResult()
    ){resul->
        if (resul.data != null && resul.resultCode == MoveForResultActivity.RESULT_CODE){
            val value =resul.data?.getIntExtra(MoveForResultActivity.
            EXTRA_SELECT_VALUE,0)
            tvResult.text ="Hasil : $value"
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvResult =findViewById(R.id.tvHasil)


        val btnPindahActivity:Button = findViewById(R.id.btnPindahActivity)
        btnPindahActivity.setOnClickListener(this)

        val btnpindahactivitydengandata:Button =findViewById(R.id.btnPindahActivitydenganData)
        btnpindahactivitydengandata.setOnClickListener(this)

        val btnPindahActivitydenganobject:Button = findViewById(R.id.btnPindahActivitydenganobject)
        btnPindahActivitydenganobject.setOnClickListener(this)

        val btnDialNumber :Button=findViewById(R.id.btnDialNumber)
        btnDialNumber.setOnClickListener(this)

        val btnPindahActivityuntukResult :Button=findViewById(R.id.btnPindahActivityuntukResult)
        btnPindahActivityuntukResult.setOnClickListener(this)


    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.btnPindahActivity){
            val intent = Intent(this,MoveActivity::class.java)
            startActivity(intent)
        }
        else if (view?.id == R.id.btnPindahActivitydenganData){
            val intent = Intent (this,MoveWithDataActivity::class.java)
            intent.putExtra(MoveWithDataActivity.EXTRA_NAME,"IMAS")
            intent.putExtra(MoveWithDataActivity.EXTRA_AGE,22)
            startActivity(intent)
        }
        else if (view?.id ==R.id.btnPindahActivitydenganobject){
            val imas = Person("Imas",
                22,
                "imasmaesharoh@gmail.com",
                "Sleman")
            val intent =Intent(this,MoveWithObjectActivity::class.java)
            intent.putExtra(MoveWithObjectActivity.KEY_OBJECT,imas)
            startActivity(intent)
        }
        else if (view?.id==R.id.btnDialNumber){
            val phone="08221524159"
            val intent=Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
            startActivity(intent)

        }
        else if (view?.id == R.id.btnPindahActivityuntukResult){
            val intent =Intent(this@MainActivity,MoveForResultActivity::class.java)
            resultLauncher.launch(intent)

        }

    }
}