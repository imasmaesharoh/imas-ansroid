package id.bootcamp.myintent

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioGroup

class MoveForResultActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_SELECT_VALUE = "EXTRA_SELECT_VALUE "
        const val RESULT_CODE = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_for_result)

        val rgPilih = findViewById<RadioGroup>(R.id.angka)
//        val angka50 = findViewById(R.id.angka50)
//        val angka100 = findViewById(R.id.angka100)
//        val angka150= findViewById(R.id.angka150)
//        val angka200= findViewById(R.id.angka200)

        val btnPilih= findViewById<Button>(R.id.pilih)
        var value = 0
        btnPilih.setOnClickListener {
            if (rgPilih.checkedRadioButtonId > 0) {
                when (rgPilih.checkedRadioButtonId) {
                    R.id.angka50 -> value = 50
                    R.id.angka100 -> value = 100
                    R.id.angka150 -> value = 150
                    R.id.angka200 -> value = 200

                }
                val resultIntent = Intent()
                resultIntent.putExtra(EXTRA_SELECT_VALUE, value)
                setResult(RESULT_CODE, resultIntent)
                finish()// menutup activity
            }
        }
    }
}