package com.example.mynetworkingapi.data.response

import com.google.gson.annotations.SerializedName

data class QuoteRespons(

	@field:SerializedName("QuoteRespons")
	val quoteRespons: List<QuoteResponsItem>
)

data class QuoteResponsItem(

	@field:SerializedName("author")
	val author: String,

	@field:SerializedName("rating")
	val rating: Any,

	@field:SerializedName("en")
	val en: String,

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("sr")
	val sr: String
)
