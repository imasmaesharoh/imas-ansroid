package com.example.mynetworkingapi.data.retrofit



import com.example.mynetworkingapi.data.response.QuoteResponsItem
import com.example.mynetworkingapi.data.response.RandomQuoteRespons
import retrofit2.Call
import retrofit2.http.GET


interface ApiService {
    @GET("/random")
    fun getRandomQuote(): Call<RandomQuoteRespons>

    @GET("/list")
    fun getRandomList():Call<List<QuoteResponsItem>>
}