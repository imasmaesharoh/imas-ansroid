package com.example.mynetworkingapi.data.response

import com.google.gson.annotations.SerializedName

data class RandomQuoteRespons(

	@field:SerializedName("author")
	val author: String,

	@field:SerializedName("en")
	val en: String,

	@field:SerializedName("id")
	val id: String
)
