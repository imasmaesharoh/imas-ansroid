package com.example.myappbar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.myappbar.databinding.ActivityMainBinding
import com.example.myappbar.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {
    lateinit var binding: ActivityMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.serchView.setupWithSearchBar(binding.searchBar)

        binding.serchView.editText.setOnEditorActionListener {
            textView, i, keyEvent ->
            binding.searchBar.setText(binding.serchView.text)
            binding.serchView.hide()
            Toast.makeText(this@MenuActivity,binding.serchView.text,
                Toast.LENGTH_LONG).show()
            false
        }
    }
}