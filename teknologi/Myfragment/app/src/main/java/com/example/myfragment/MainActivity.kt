package com.example.myfragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Memasang Fragment

        val fragmentManager = supportFragmentManager
        val homeFragment = HomeFragment()
        val fragment = fragmentManager.findFragmentByTag(HomeFragment :: class.java.simpleName)
        if (fragment !is HomeFragment){
            //Masang Fragment
            fragmentManager
                .beginTransaction()
                .add(R.id.frameContainer,homeFragment,HomeFragment :: class.java.simpleName)
                .commit()
        }
    }
}