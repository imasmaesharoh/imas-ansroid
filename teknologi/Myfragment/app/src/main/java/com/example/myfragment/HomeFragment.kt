package com.example.myfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(), View.OnClickListener {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnCategory: Button= view.findViewById(R.id.btnCatagory)
        btnCategory.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnCatagory){
            //Logic untuk pindah ke CategoryFragment ketika tombol di klik
            val categoryFragment = CategoryFragment()
            val fragmentManager =parentFragmentManager
            fragmentManager
                .beginTransaction()
                .replace(R.id.frameContainer,categoryFragment
                    ,CategoryFragment :: class.java.simpleName)
                .addToBackStack(null)
                .commit()
        }

    }
}