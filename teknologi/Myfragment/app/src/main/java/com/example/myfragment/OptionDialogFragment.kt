package com.example.myfragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.fragment.app.DialogFragment

/**
 * A simple [Fragment] subclass.
 * Use the [OptionDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OptionDialogFragment : DialogFragment() {

    private lateinit var btnChooose : Button
    private lateinit var btnClose : Button
    private lateinit var rgChoose : RadioGroup
    private  lateinit var rb1 :RadioButton
    private  lateinit var rb2 :RadioButton
    private  lateinit var rb3 :RadioButton

    var onOptionDialogListener : OnOptionDialogListener? = null

    interface OnOptionDialogListener{
        fun onOptionDialogListener(text:String)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnChooose = view.findViewById(R.id.btnPilih)
        btnClose = view.findViewById(R.id.btnTutup)
        rgChoose=view.findViewById(R.id.rgGroup)
        rb1=view.findViewById(R.id.rbMomo)
        rb2=view.findViewById(R.id.rbMolly)
        rb3=view.findViewById(R.id.rbMocha)

        btnClose.setOnClickListener {
            dialog?.dismiss()
        }
        btnChooose.setOnClickListener {
            val checkRadioButtonId =rgChoose.checkedRadioButtonId
            if (checkRadioButtonId != -1){
                var text =""
                when(checkRadioButtonId){
                    R.id.rbMomo -> text =rb1.text.toString().trim()
                    R.id.rbMolly -> text =rb2.text.toString().trim()
                    R.id.rbMocha -> text =rb3.text.toString().trim()
                }
                //Trigger
                onOptionDialogListener?.onOptionDialogListener(text)
                dialog?.dismiss()


            }
        }
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_option_dialog, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val fragment =parentFragment
        if (fragment is DetailCategoryFragment){
            //Pasangan Listener/Setup Listener
            this.onOptionDialogListener = fragment.optionDialogListener
        }
    }

    override fun onDetach() {
        super.onDetach()
        //Bersih-bersoh
        onOptionDialogListener = null
    }
}