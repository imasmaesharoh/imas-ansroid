package com.example.myfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentManager


/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment(), View.OnClickListener {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnDetailCategory : Button = view.findViewById(R.id.btnLifesyle)
        btnDetailCategory.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.btnLifesyle ){
            val detailCategoryFragment = DetailCategoryFragment()
            val bundle = Bundle()
            bundle.putString(DetailCategoryFragment.EXTRA_NAME,"Foods & beverages")
            val description= "Kategory ini akan berisi produk foods & beverages"

            detailCategoryFragment.arguments = bundle
            detailCategoryFragment.description =description

            val fragmentManager = parentFragmentManager
            fragmentManager
                .beginTransaction()
                .replace(R.id.frameContainer,detailCategoryFragment
                ,DetailCategoryFragment :: class.java.simpleName)
                .addToBackStack(null)
                .commit()
        }
    }

}