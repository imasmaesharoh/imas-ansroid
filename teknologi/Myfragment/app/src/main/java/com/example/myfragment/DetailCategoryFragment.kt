package com.example.myfragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


/**
 * A simple [Fragment] subclass.
 * Use the [DetailCategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailCategoryFragment : Fragment() {
    private lateinit var tvCategoryName : TextView
    private lateinit var tvCategoryDesc : TextView
    private lateinit var btnProfile : Button
    private lateinit var btnShowDialog : Button

    var description : String? = null
    companion object{
        const val EXTRA_NAME = "EXTRA_NAME"
        const val EXTRA_DESC = "EXTRA_DESC"
    }


    var optionDialogListener : OptionDialogFragment.OnOptionDialogListener =
        object  : OptionDialogFragment.OnOptionDialogListener{
            override fun onOptionDialogListener(text: String) {

                //Aksi ketika optionDialalogListener ketrigger
                Toast.makeText(requireContext(), "Anda Memilih $text",
                Toast.LENGTH_SHORT).show()
            }

        }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvCategoryName = view.findViewById(R.id.tvlifestyle)
        tvCategoryDesc= view.findViewById(R.id.tvProdukLifestyle)
        btnProfile = view.findViewById(R.id.btnProfileActivity)
        btnShowDialog = view.findViewById(R.id.btnDialog)


        if (savedInstanceState !=null){
            val descBundle =savedInstanceState.getString(EXTRA_DESC)
            description = descBundle
        }
        if (arguments != null){
            val categoryName = arguments?.getString(EXTRA_NAME)
            tvCategoryName.text=categoryName
            tvCategoryDesc.text=description
        }
        btnShowDialog.setOnClickListener {
            val optionDialogFragment = OptionDialogFragment()
            val fragmenManager =childFragmentManager
            optionDialogFragment.show(fragmenManager,OptionDialogFragment :: class.java.simpleName)
        }

        btnProfile.setOnClickListener {
            val intent =Intent(requireContext(),ProfileActivity :: class.java)
            startActivity(intent)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EXTRA_DESC,description)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_category, container, false)

    }
}