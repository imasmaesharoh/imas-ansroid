package com.example.mynetworkingapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView.ItemView
import androidx.recyclerview.widget.RecyclerView

class QouteAdapter(val dataList : ArrayList<Qoute>) : RecyclerView.Adapter<QouteAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
        val tvQoute = itemView.findViewById<TextView>(R.id.txtQoutes)
        val tvAuthor = itemView.findViewById<TextView>(R.id.txtAutohor)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QouteAdapter.ViewHolder {
      val view = LayoutInflater.from(parent.context).inflate(R.layout.item_quotes,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: QouteAdapter.ViewHolder, position: Int) {
     val data = dataList [position]
        holder.tvQoute.text = data.qoute
        holder.tvAuthor.text = data.author
    }

    override fun getItemCount(): Int {
       return  dataList.size
    }
}