package com.example.mynetworkingapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mynetworkingapi.databinding.ActivityQouteListBinding
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONArray

class QouteListActivity : AppCompatActivity() {

    private  lateinit var binding: ActivityQouteListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qoute_list)
        binding = ActivityQouteListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getListQuote()
    }

    private fun getListQuote() {
        binding.progressBar.visibility = View.VISIBLE
        val client =AsyncHttpClient()
        val url ="https://quote-api.dicoding.dev/list"

        client.get(url,object  : AsyncHttpResponseHandler (){
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
               binding.progressBar.visibility =View.INVISIBLE

                val result = String(responseBody as ByteArray)

                val jsonArray = JSONArray(result)// Karen tutupnya Array []

                val arrayQoute = ArrayList<Qoute>()
                for (i in 0 until  jsonArray.length()){
                    val jsonObject = jsonArray.getJSONObject(i)

                    val quote = jsonObject.getString("en")
                    val author = jsonObject.getString("author")
                    val objQuote =Qoute(quote,author)
                    arrayQoute.add(objQuote)

                }
                binding.rvList.layoutManager =
                    LinearLayoutManager(this@QouteListActivity)
                binding.rvList.adapter = QouteAdapter(arrayQoute)
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                binding.progressBar.visibility = View.INVISIBLE
                val errorMsg = error?.message
                Toast.makeText(this@QouteListActivity,errorMsg,Toast.LENGTH_SHORT).show()
            }

        }
        )
    }
}