package com.example.mynetworkingapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.mynetworkingapi.databinding.ActivityMainBinding
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private  lateinit var  binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getRandomQoute()

        binding.button.setOnClickListener {
            val intent = Intent(this,QouteListActivity::class.java)
            startActivity(intent)
        }
    }
    private fun getRandomQoute(){
        //Logic ambil dari data API
        binding.pbBar.visibility =View.VISIBLE
        val client = AsyncHttpClient()
        val url ="https://quote-api.dicoding.dev/random"
        client.get(url,object : AsyncHttpResponseHandler(){
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                binding.pbBar.visibility =View.INVISIBLE
                val result = String(responseBody as ByteArray)

                val responseObject = JSONObject(result)

                val quote = responseObject.getString("en")
                val author = responseObject.getString("author")

                binding.tvQoutes.text =quote
                binding.tvName.text = author

            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                binding.pbBar.visibility =View.INVISIBLE
                Toast.makeText(this@MainActivity,error?.message,Toast.LENGTH_SHORT)
                    .show()

            }

        }
        )
    }
}