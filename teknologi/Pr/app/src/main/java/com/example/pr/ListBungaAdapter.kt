package com.example.pr

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


class ListBungaAdapter(private val listBunga : ArrayList<Bunga>): RecyclerView.Adapter<ListBungaAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback
    interface OnItemClickCallback {
        fun onitemClickCalled (data:Bunga)
    }
    fun setOnClickCallback (onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }


    class ListViewHolder(itemView : View)//mewakili setiap item
        : RecyclerView.ViewHolder(itemView){

        val imgPhoto= itemView.findViewById<ImageView>(R.id.imgBunga)
        val  textName = itemView.findViewById<TextView>(R.id.tvName)
        val textDesc= itemView.findViewById<TextView>(R.id.tvDesc)



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view:View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_bunga,parent,false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listBunga.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val data= listBunga[position]
       Glide.with(holder.itemView.context).load(data.imgBunga).into(holder.imgPhoto)
        holder.textName.text = data.nama_bunga
        holder.textDesc.text =data.nama_desc
        holder.itemView.setOnClickListener {
            onItemClickCallback.onitemClickCalled(data)

        }



    }

}