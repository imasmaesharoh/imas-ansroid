package com.example.pr

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    // 1  deklarasi
    private lateinit var rvBunga : RecyclerView
    private val bungalist : ArrayList<Bunga> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {//2
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        rvBunga = findViewById(R.id.rvBunga)
        //3
       bungalist.addAll(getBungaList())
        //5
        showRecyclerView()
    }
    private  fun getBungaList():ArrayList<Bunga>{
        val dataName =resources.getStringArray(R.array.data_name)
        val dataDesc =resources.getStringArray(R.array.data_description)
        val dataPhoto =resources.getStringArray(R.array.data_photo)
        val dataSpesifikasi = resources.getStringArray(R.array.data_Spesies)
        val listBunga =ArrayList<Bunga>()

        for (i in 0.. dataName.lastIndex){
            val bunga = Bunga(dataName[i],dataDesc[i],dataPhoto[i], dataSpesifikasi[i])
            listBunga.add(bunga)
        }
        return listBunga
    }

    private fun showRecyclerView() {
        //6
        rvBunga.layoutManager = LinearLayoutManager (this)
        val listBungaAdapter = ListBungaAdapter(bungalist)
        listBungaAdapter.setOnClickCallback(object : ListBungaAdapter.OnItemClickCallback {
            override fun onitemClickCalled(data: Bunga) {
                val intent = Intent(this@MainActivity,DetailBunga::class.java)
                val name = intent.putExtra(DetailBunga.KEY_NAME,data.nama_bunga)
                val desc = intent.putExtra(DetailBunga.KEY_DESC,data.nama_desc)
                val spes = intent.putExtra(DetailBunga.KEY_SPESIES,data.nama_spesifikasi)
                val photo = intent.putExtra(DetailBunga.KEY_PHOTO,data.imgBunga)
                val photo1 = intent.putExtra(DetailBunga.KEY_PHOTO,data.imgBunga)
                
                startActivity(intent)

            }
        })
        rvBunga.adapter =listBungaAdapter

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_main,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu1 -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}