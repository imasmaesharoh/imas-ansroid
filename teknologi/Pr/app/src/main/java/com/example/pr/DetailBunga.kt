package com.example.pr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class DetailBunga : AppCompatActivity() {

  companion object{
      val KEY_NAME = "KEY_NAME"
      val KEY_DESC = "KEY_DESC"
      val KEY_SPESIES = "KEY_SPESIES"
      val KEY_PHOTO = "KEY_PHOTO"
  }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_bunga)

        val nama = findViewById<TextView>(R.id.tvBunga)
        val desc = findViewById<TextView>(R.id.tvDesc1)
        val spes= findViewById<TextView>(R.id.tvSpes)
        val photo = findViewById<ImageView>(R.id.imgBunga)
        val photo1= findViewById<ImageView>(R.id.imgBunga2)

        val  name =intent.getStringExtra(KEY_NAME)
        val  desc1 =intent.getStringExtra(KEY_DESC)
        val  spes1 =intent.getStringExtra(KEY_SPESIES)
        val  photo0=intent.getStringExtra(KEY_PHOTO)
        val  photo11 =intent.getStringExtra(KEY_PHOTO)

      nama.text = name
      desc.text = desc1
      spes.text = spes1
        Glide.with(this).load(photo11).into(photo1)
        Glide.with(this).load(photo0).into(photo)
    }
}