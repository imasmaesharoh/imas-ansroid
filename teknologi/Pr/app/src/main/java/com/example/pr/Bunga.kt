package com.example.pr

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

//@Parcelize
data class Bunga(
    val nama_bunga : String,
    val nama_desc : String,
    val imgBunga : String,
    val nama_spesifikasi:String
)//:Parcelable
