package id.bootcamp.barvolumw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //Deklarasi Objek yang belum di inisialisasi
    private lateinit var etpanjang : EditText
    private lateinit var etlebar : EditText
    private lateinit var ettingg :EditText
    private lateinit var btnhitung : Button
    private lateinit var tvhasil : TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //inisiasi
        etpanjang = findViewById(R.id. btnpanjang)
        etlebar =findViewById(R.id.btnlebar)
        ettingg =findViewById(R.id.btntinggi)
        btnhitung=findViewById(R.id.btnhitung)
        tvhasil = findViewById(R.id.tvhasil)

        btnhitung.setOnClickListener(this)

        if (savedInstanceState != null){
            val result =savedInstanceState.getString(STATE_RESULT)
            tvhasil.text = result
        }


    }
    companion object{
        private const val STATE_RESULT ="state_result"

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_RESULT, tvhasil.text.toString())
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.btnhitung){
            //Ambil nilai panjang, lebar. tinggi
            val inputpanjang = etpanjang.text.toString().trim()
            val  inputlebar =etlebar.text.toString().trim()
            val inputtinggi = ettingg.text.toString().trim()

            //Validasi
            var isEmptyfields = false

            if (inputpanjang.isEmpty()){
                isEmptyfields =true
               etpanjang.error = "Fields panjang tidak boleh kosong"
            }
            if (inputlebar.isEmpty()){
                isEmptyfields = true
                etlebar.error="Fields lebar tidak boleh kosong"

            }
            if (inputtinggi.isEmpty()){
                isEmptyfields =true
                ettingg.error="Fields lebar tidak boleh kosong"
            }
            if (isEmptyfields == false){

            //Hitung volumr
            val volume =inputpanjang.toDouble() * inputlebar.toDouble() *inputtinggi.toDouble()

            //tampilkan ke text view
            tvhasil.text = volume.toString()
        }

    }
    }
}