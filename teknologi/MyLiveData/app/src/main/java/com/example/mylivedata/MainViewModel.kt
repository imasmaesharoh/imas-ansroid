package com.example.mylivedata

import android.os.SystemClock
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.Timer
import java.util.TimerTask

class MainViewModel : ViewModel() {

    companion object{
        private const val  ONE_SECOND :Long = 1000
    }
    // Mengambil waktu
    private val mInitialTime = SystemClock.elapsedRealtime()
        //Variable untuk menampung data yang sudh berlalu
     val mElapsedTime = MutableLiveData<Long>()

    init {
        val timer =  Timer()
        //Perulangan di dalam block run, dengan delay 1 detik
        timer.scheduleAtFixedRate(object  : TimerTask(){
            override fun run() {

                //Menghitung selisih waktu sekarang dan waktu tadi pas di ambil detik
             val newValue = ( SystemClock.elapsedRealtime() - mInitialTime)/1000
                mElapsedTime.postValue(newValue)
            }

        },ONE_SECOND, ONE_SECOND)

    }
}